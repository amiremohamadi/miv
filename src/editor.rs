use std::io::{self, stdout, Write};
use termion::event::Key;
use termion::input::TermRead;
use termion::raw::IntoRawMode;

type IOError = std::io::Error;

struct Position {
    pub x: usize,
    pub y: usize,
}

pub struct Editor {
    runing: bool,
    cursor: Position,
}

impl Editor {
    pub fn default() -> Self {
        Self {
            runing: true,
            cursor: Position { x: 1, y: 1 },
        }
    }

    pub fn run(&mut self) {
        let _stdout = stdout().into_raw_mode().unwrap();

        while self.runing {
            if let Err(error) = self.refresh_screen() {
                panic!(error);
            }
            if let Err(error) = self.process_keypress() {
                panic!(error);
            }
        }
    }

    fn refresh_screen(&self) -> Result<(), IOError> {
        print!("{}{}", termion::clear::All, termion::cursor::Goto(1, 1));
        self.draw_rows();
        self.move_cursor(&self.cursor);
        io::stdout().flush()
    }

    fn move_cursor(&self, position: &Position) {
        print!(
            "{}",
            termion::cursor::Goto(position.x as u16, position.y as u16)
        )
    }

    fn draw_rows(&self) {
        let size = termion::terminal_size().unwrap();
        let (_, height) = size;
        for _ in 0..height - 1 {
            println!("~\r");
        }
    }

    fn process_keypress(&mut self) -> Result<(), IOError> {
        let key = self.read_key()?;
        match key {
            Key::Ctrl('q') => self.runing = false,
            // TODO: limit cursor boundaries
            Key::Down => self.cursor.y += 1,
            Key::Up => self.cursor.y -= 1,
            Key::Left => self.cursor.x -= 1,
            Key::Right => self.cursor.x += 1,
            _ => (),
        }

        Ok(())
    }

    fn read_key(&self) -> Result<Key, IOError> {
        loop {
            if let Some(key) = io::stdin().lock().keys().next() {
                return key;
            }
        }
    }
}
